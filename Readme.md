Android App zur Buchung von Shared-Taxen
========================================

Dieses Repository ist im Rahmen des Seminars "Event Processing" des
Forschungszentrum Informatik (FZI) im Sommersemster 2016 am 
Karlsruher Institut für Technologie (KIT) entstanden.

Von Tim Bossenmaier, Nevena Nikolajevic und Adrian Vetter.


### Verwendung
1. Android Studio öffnen.
2. Projekt mit **File** -> **New** -> **Project from Version Control** -> **Git** importieren.
3. In der _SearchActivity.java_ die Adresse und den Port zum [TaxiServer](https://bitbucket.org/eventprocessingkit/taxiserver) angeben.
4. Mit Gradle kompilieren.