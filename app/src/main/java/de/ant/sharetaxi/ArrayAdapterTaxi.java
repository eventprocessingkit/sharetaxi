package de.ant.sharetaxi;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import de.ant.sharetaxi.helper.DistanceCalculator;

/**
 * Created for the seminar "Event Processing" SoSe 2016
 * at the Forschungszentrum Informatik (FZI) in Karlsruhe
 * by
 * Tim Bossenmaier
 * Nevena Nikolajevic and
 * Adrian Vetter
 *
 * ArrayAdapter to fill the results screen with data from the taxi routes
 */
public class ArrayAdapterTaxi extends ArrayAdapter <TaxiResult> {

    public static final int MILES_IN_YARDS = 1760;

    private ResultsActivity resultsActivity;
    private Address addressStart;
    private Address addressEnd;

    public ArrayAdapterTaxi(ResultsActivity resultsActivity, List <TaxiResult> taxiResults){
        super(resultsActivity, R.layout.list_item_taxiliste, taxiResults);
        this.resultsActivity = resultsActivity;
        this.addressStart = resultsActivity.addressStart;
        this.addressEnd = resultsActivity.addressEnd;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent){

        // Get current taxiResult
        TaxiResult taxiResult = getItem(position);

        // Get colors from xml
        int orangeColor = getContext().getResources().getColor(R.color.colorOrange);
        int greenColor = getContext().getResources().getColor(R.color.colorGreen);
        int redColor = getContext().getResources().getColor(R.color.colorRed);

        // Erhalte Referenzen auf die Layoutbestandteile
        LayoutInflater inflater = (LayoutInflater) resultsActivity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View row = inflater.inflate(R.layout.list_item_taxiliste, parent, false);

        // Get layout elements
        TextView timeTextView = (TextView) row.findViewById(R.id.pickUp_time);
        TextView seatsTextView = (TextView) row.findViewById(R.id.available_seats);
        TextView seatsLabelTextView = (TextView) row.findViewById(R.id.label_seats);
        TextView actualPriceTextView = (TextView) row.findViewById(R.id.your_price);
        TextView divergenceTextView = (TextView) row.findViewById(R.id.divergence_pickUp) ;
        TextView regularPriceTextView = (TextView) row.findViewById(R.id.regular_price);
        Button bookItButton = (Button) row.findViewById(R.id.button_bookIt);

        // Set listener
        bookItButton.setOnClickListener(new ButtonListener(taxiResult));


        // Hinzufügen der verfügbaren Sitze
        int available_seats = taxiResult.getAvailableSeats();
        switch (available_seats){
            case 0: seatsTextView.setTextColor(redColor);
                seatsLabelTextView.setText("Seats");
                break;
            case 1: seatsTextView.setTextColor(orangeColor);
                seatsLabelTextView.setText("Seat");
                break;
            default: seatsTextView.setTextColor(greenColor);
                seatsLabelTextView.setText("Seats");
                break;
        }

        // Set color based on relative distance to input divergence
        float divergence = (float)DistanceCalculator.distance(addressStart.getLatitude(), addressStart.getLongitude(), taxiResult.getTaxiStartPosLat(), taxiResult.getTaxiStartPosLon(), "M");
        if (divergence <= 0.333 * resultsActivity.divergence)
            divergenceTextView.setTextColor(greenColor);
        else if (divergence <= 0.666 * resultsActivity.divergence)
            divergenceTextView.setTextColor(orangeColor);
        else
            divergenceTextView.setTextColor(redColor);

        divergenceTextView.setText(String.format("%.0f yards", divergence * MILES_IN_YARDS));

        // Aufbereitung der Startzeit
        Calendar pickUpTime = taxiResult.getStartTime();
        SimpleDateFormat sf = new SimpleDateFormat(" h':'mm a");

        timeTextView.setText(sf.format(pickUpTime.getTime()));
        seatsTextView.setText(Integer.toString(available_seats));

        // Get and set values
        float actualPrice = taxiResult.getActualPrice();
        float regularPrice = taxiResult.getNormalPrice();
        actualPriceTextView.setText(Float.toString(actualPrice) + " $");
        regularPriceTextView.setText(Float.toString(regularPrice) + " $");

        return row;
    }

    /**
     * Listener helper class
     */
    protected class ButtonListener implements View.OnClickListener {

        private TaxiResult taxiResult;

        public ButtonListener( TaxiResult taxiResult) {
            this.taxiResult = taxiResult;
        }

        @Override
        public void onClick(View v) {

            Intent myIntent = new Intent(resultsActivity.getBaseContext(), DetailsActivity.class);
            myIntent.putExtra("AddressStart", addressStart);
            myIntent.putExtra("AddressEnd", addressEnd);
            myIntent.putExtra("TaxiResult", taxiResult);

            // Start new activity
            resultsActivity.startActivity(myIntent);
        }
    }
}
