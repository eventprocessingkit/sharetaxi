package de.ant.sharetaxi.helper;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import de.ant.sharetaxi.R;

/**
 * Created for the seminar "Event Processing" SoSe 2016
 * at the Forschungszentrum Informatik (FZI) in Karlsruhe
 * by
 * Tim Bossenmaier
 * Nevena Nikolajevic and
 * Adrian Vetter
 *
 * Handler class for managing the DatePicker
 */
public class DateDialogHandler extends DialogFragment {

    /**
     * US Date format
     */
    public static SimpleDateFormat US_DATE_FORMAT = new SimpleDateFormat("MM/dd/yy");

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState){

        // Set listener and start value
        DateSetListener dateSetListener = new DateSetListener(getActivity());
        DatePickerDialog dialog = new DatePickerDialog(getActivity(), dateSetListener, 2013, 0, 1);

        // Set range to test set
        Calendar minCal = new GregorianCalendar(2013,Calendar.JANUARY,1);
        Calendar maxCal = new GregorianCalendar(2013,Calendar.JANUARY,20);
        dialog.getDatePicker().setMinDate(minCal.getTimeInMillis());
        dialog.getDatePicker().setMaxDate(maxCal.getTimeInMillis());

        return dialog;
    }

    /**
     * Listener helper class
     */
    protected class DateSetListener implements DatePickerDialog.OnDateSetListener {

        private Activity activity;

        public DateSetListener(Activity activity) {
            this.activity = activity;
        }

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            TextView dateTextView = (TextView) activity.findViewById(R.id.editDate);

            // Set text in date field
            Date pickedDate = new Date(year - 1900, monthOfYear, dayOfMonth);
            dateTextView.setText(US_DATE_FORMAT.format(pickedDate));
        }
    }
}
