package de.ant.sharetaxi.helper;

import android.app.Activity;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.TextView;
import android.widget.TimePicker;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import de.ant.sharetaxi.R;

/**
 * Created for the seminar "Event Processing" SoSe 2016
 * at the Forschungszentrum Informatik (FZI) in Karlsruhe
 * by
 * Tim Bossenmaier
 * Nevena Nikolajevic and
 * Adrian Vetter
 *
 * Handler class for managing the TimePicker
 */
public class TimeDialogHandler extends DialogFragment {

    /**
     * US Time format
     */
    public static SimpleDateFormat US_TIME_FORMAT = new SimpleDateFormat("h:mm a");

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        Calendar calendar = Calendar.getInstance();
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);
        TimePickerDialog dialog;
        TimeSetListener timeSetListener = new TimeSetListener(getActivity());
        dialog = new TimePickerDialog(getActivity(), timeSetListener, hour, minute, false);

        return dialog;
    }

    /**
     * Listener helper class
     */
    protected class TimeSetListener implements TimePickerDialog.OnTimeSetListener {

        private Activity activity;

        public TimeSetListener(Activity activity){
            this.activity = activity;
        }

        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            TextView time = (TextView) activity.findViewById(R.id.editTime);

            // Set text in date field
            Date pickedDate = new Date(2013, 0, 1, hourOfDay, minute);
            time.setText(US_TIME_FORMAT.format(pickedDate));
        }
    }
}
