package de.ant.sharetaxi;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;

import android.os.AsyncTask;

import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import de.ant.sharetaxi.helper.TimeDialogHandler;
import de.ant.sharetaxi.helper.DateDialogHandler;
import de.ant.sharetaxi.helper.PlacesAutoCompleteAdapter;


/**
 * Created for the seminar "Event Processing" SoSe 2016
 * at the Forschungszentrum Informatik (FZI) in Karlsruhe
 * by
 * Tim Bossenmaier
 * Nevena Nikolajevic and
 * Adrian Vetter
 *
 * The MainActivity of the app, showing the search screen
 */
public class SearchActivity extends AppCompatActivity {

    // Set server address and port
    private static final String SERVER_ADDRESS  = "kalmar39.fzi.de";
    private static final int    SERVER_PORT     = 80;


    /**
     * Keep track of the login task to ensure we can cancel it if requested.
     */
    private FindTaxiTask mSearchTask = null;

    // UI references.
    private AutoCompleteTextView mPickupView;
    private AutoCompleteTextView mDropOffView;
    private View mProgressView;
    private View mLoginFormView;
    private TextView mStartDateView;
    private TextView mStartTimeView;
    private SeekBar mDivergenceSeekBar;

    private GoogleApiClient mGoogleApiClient;
    private PlacesAutoCompleteAdapter mPlacesAdapter;
    private static final LatLngBounds BOUNDS_GREATER_NEW_YORK = new LatLngBounds(new LatLng(40.477399, -73.700272), new LatLng(40.917577, -74.259090));


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        // Set up the search form
        mPickupView = (AutoCompleteTextView) findViewById(R.id.taxi_pickup);
        mStartDateView = (TextView) findViewById(R.id.editDate);
        mStartTimeView = (TextView) findViewById(R.id.editTime);
        mDivergenceSeekBar = (SeekBar) findViewById(R.id.SeekBarDivergencePickUp);
        mDivergenceSeekBar.setOnSeekBarChangeListener(new SeekBarListener());
        mPickupView.setImeOptions(EditorInfo.IME_ACTION_NEXT);
        mPickupView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_NEXT || id == EditorInfo.IME_NULL) {
                    mDropOffView.requestFocus();
                    return true;
                }
                return false;
            }
        });

        mDropOffView = (AutoCompleteTextView) findViewById(R.id.taxi_dropoff);
        mDropOffView.setImeOptions(EditorInfo.IME_ACTION_SEARCH);
        mDropOffView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_SEARCH || id == EditorInfo.IME_NULL) {
                    attemptSearch();
                    return true;
                }
                return false;
            }
        });

        Button mSearchButton = (Button) findViewById(R.id.search_button);
        mSearchButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptSearch();
            }
        });

        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);


        // Create API Client
        mGoogleApiClient = new GoogleApiClient
                .Builder(this)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .addApi(AppIndex.API).build();

        // Init adapter
        mPlacesAdapter = new PlacesAutoCompleteAdapter(this, android.R.layout.simple_list_item_1,
                mGoogleApiClient, BOUNDS_GREATER_NEW_YORK, null);

        // Set adapters for autocomplete
        mPickupView.setOnItemClickListener(mAutocompleteClickListener);
        mPickupView.setAdapter(mPlacesAdapter);
        mDropOffView.setOnItemClickListener(mAutocompleteClickListener);
        mDropOffView.setAdapter(mPlacesAdapter);
    }


    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptSearch() {
        if (mSearchTask != null) {
            return;
        }

        // Reset errors.
        mPickupView.setError(null);
        mDropOffView.setError(null);

        // Store values at the time of the search attempt
        String pickup = mPickupView.getText().toString();
        String dropOff = mDropOffView.getText().toString();

        String inputDate = mStartDateView.getText().toString();
        String inputTime = mStartTimeView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid start
        if (TextUtils.isEmpty(pickup)) {
            mPickupView.setError(getString(R.string.error_field_required));
            focusView = mPickupView;
            cancel = true;
        }

        // Check for a valid drop off, if the user entered one
        if (TextUtils.isEmpty(dropOff)) {
            mDropOffView.setError(getString(R.string.error_field_required));
            if (focusView == null)
                focusView = mDropOffView;
            cancel = true;
        }

        // Check for a valid date, if the user entered one
        if (TextUtils.isEmpty(inputDate)) {
            mStartDateView.setError(getString(R.string.error_field_required));
            if (focusView == null)
                focusView = mStartDateView;
            cancel = true;
        }

        // Check for a valid date, if the user entered one
        if (TextUtils.isEmpty(inputTime)) {
            mStartTimeView.setError(getString(R.string.error_field_required));
            if (focusView == null)
                focusView = mStartTimeView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error
            focusView.requestFocus();
        } else {

            // Format Time and Date
            Date startDate = null;
            Date startTime = null;
            try {
                startDate = DateDialogHandler.US_DATE_FORMAT.parse(inputDate);
                startTime = TimeDialogHandler.US_TIME_FORMAT.parse(inputTime);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            // SQL time/date format
            SimpleDateFormat dbDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat dbTimeFormat = new SimpleDateFormat("HH:mm:ss");
            String startDateTime = dbDateFormat.format(startDate) + " " +  dbTimeFormat.format(startTime);

            // Divergence from PickUp in absolute miles
            float divergence = ((float) mDivergenceSeekBar.getProgress()) / 100;

            // Show a progress spinner, and kick off a background task to
            // perform the search
            showProgress(true);
            mSearchTask = new FindTaxiTask(pickup, dropOff, startDateTime, divergence);
            mSearchTask.execute((Void) null);
        }
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }

    public void setDate (View view){
        DateDialogHandler dateDialogHandler = new DateDialogHandler();
        dateDialogHandler.show(getSupportFragmentManager(), "date_picker");
    }

    public void setTime(View view){
        TimeDialogHandler timeDialogHandler = new TimeDialogHandler();
        timeDialogHandler.show(getSupportFragmentManager(), "time_picker");
    }

    private AdapterView.OnItemClickListener mAutocompleteClickListener
            = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            final PlacesAutoCompleteAdapter.PlaceAutocomplete item = mPlacesAdapter.getItem(position);
            final String placeId = String.valueOf(item.placeId);
            PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi
                    .getPlaceById(mGoogleApiClient, placeId);
            placeResult.setResultCallback(mUpdatePlaceDetailsCallback);
        }
    };

    private ResultCallback<PlaceBuffer> mUpdatePlaceDetailsCallback
            = new ResultCallback<PlaceBuffer>() {
        @Override
        public void onResult(PlaceBuffer places) {
            if (!places.getStatus().isSuccess()) {
                Log.e("place", "Place query did not complete. Error: " +
                        places.getStatus().toString());
                places.release();
                return;
            }
            // Selecting the first object buffer.
            final Place place = places.get(0);
            places.release();
        }
    };

    /**
     * Represents an asynchronous search task used to find available taxis.
     */
    public class FindTaxiTask extends AsyncTask<Void, Void, Boolean> {

        private String start;
        private String end;
        private Address addressStart;
        private Address addressEnd;

        private String startDateTime;
        private float divergence;

        private ArrayList<TaxiResult> results;

        FindTaxiTask(String start, String end, String startDateTime, float divergence) {
            this.start = start;
            this.end = end;
            this.startDateTime = startDateTime;
            this.divergence = divergence;
        }

        @Override
        protected Boolean doInBackground(Void... params) {

            // Get coordinates from Google API
            Log.d("FindTaxiTask", "Start: " + start);
            Log.d("FindTaxiTask", "End: " + end);

            Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
            List<Address> addressesStart;
            List<Address> addressesEnd;
            try {
                addressesStart = geocoder.getFromLocationName(start, 1, BOUNDS_GREATER_NEW_YORK.southwest.latitude,
                        BOUNDS_GREATER_NEW_YORK.southwest.longitude, BOUNDS_GREATER_NEW_YORK.northeast.latitude, BOUNDS_GREATER_NEW_YORK.northeast.longitude);
                addressesEnd = geocoder.getFromLocationName(end, 1, BOUNDS_GREATER_NEW_YORK.southwest.latitude,
                        BOUNDS_GREATER_NEW_YORK.southwest.longitude, BOUNDS_GREATER_NEW_YORK.northeast.latitude, BOUNDS_GREATER_NEW_YORK.northeast.longitude);
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }

            // Check if there are results
            if (addressesStart == null || addressesStart.size() == 0
                    || addressesEnd == null || addressesEnd.size() == 0 )
                return false;

            // Store start/end address
            addressStart = addressesStart.get(0);
            addressEnd = addressesEnd.get(0);
            Log.d("FindTaxiTask", "Coordinates acquired");

            /*
                1. Look for taxi with same start/end
                    2.1 If no Taxi was found -> store request on server (like game queue) "Waiting for more ppl"
                    2.1 If Taxi was found -> Get results of available taxis
             */
            Log.d("FindTaxiTask","Connect to server");
            try {

                Socket socket = new Socket(SERVER_ADDRESS, SERVER_PORT);
                PrintWriter out = new PrintWriter(socket.getOutputStream(), true);

                // Send message to server
                out.println(startDateTime
                        + "," + divergence
                        + "," + addressStart.getLatitude()
                        + "," + addressStart.getLongitude()
                        + "," + addressEnd.getLatitude()
                        + "," + addressEnd.getLongitude());


                BufferedReader input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                results = new ArrayList<>();
                String line;
                while ((line = input.readLine()) != null) {
                    results.add(new TaxiResult(line));
                }

                Log.d("FindTaxiTask", "Received " + results.size() + " available taxis");

                input.close();
                socket.close();
            }catch(IOException e) {
                e.printStackTrace();
            }

            return true;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mSearchTask = null;

            if (success) {
                // Open new activity
                Intent myIntent = new Intent(getBaseContext(), ResultsActivity.class);
                myIntent.putExtra("Results", results);
                myIntent.putExtra("AddressStart", addressStart);
                myIntent.putExtra("AddressEnd", addressEnd);
                myIntent.putExtra("AddressStartString", start);
                myIntent.putExtra("AddressEndString", end);
                myIntent.putExtra("Divergence", divergence);

                startActivity(myIntent);
                showProgress(false);
            } else {
                showProgress(false);
                mDropOffView.setError(getString(R.string.error_xy));
                mDropOffView.requestFocus();
            }
        }

        @Override
        protected void onCancelled() {
            mSearchTask = null;
            showProgress(false);
        }
    }


    @SuppressLint("ValidFragment")
    public class SeekBarListener implements SeekBar.OnSeekBarChangeListener {

        public SeekBarListener() {
        }

        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            TextView actualDivergence = (TextView) findViewById(R.id.actualDivergence);
            switch(progress){
                case 100: actualDivergence.setText("1 mile");break;
                default: actualDivergence.setText(Math.round((progress*ArrayAdapterTaxi.MILES_IN_YARDS)/100.0) + " yards");break;
            }
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {

        }
    }
}


