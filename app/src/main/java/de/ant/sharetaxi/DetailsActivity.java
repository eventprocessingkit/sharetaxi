package de.ant.sharetaxi;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.directions.route.AbstractRouting;
import com.directions.route.Route;
import com.directions.route.RouteException;
import com.directions.route.Routing;
import com.directions.route.RoutingListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import de.ant.sharetaxi.helper.DistanceCalculator;


/**
 * Created for the seminar "Event Processing" SoSe 2016
 * at the Forschungszentrum Informatik (FZI) in Karlsruhe
 * by
 * Tim Bossenmaier
 * Nevena Nikolajevic and
 * Adrian Vetter
 *
 * The Activity for the details screen
 */
public class DetailsActivity extends AppCompatActivity implements OnMapReadyCallback {

    /**
     * Location request ID
     */
    private static final int LOCATION_REQUEST = 1;

    private GoogleMap mMap;

    private Address addressStart;
    private Address addressEnd;
    private TaxiResult taxiResult;
    private TextView timeTextView;
    private TextView seatsTextView;
    private TextView divergenceTextView;
    private TextView actualPriceTextView;
    private TextView regularPriceTextView;
    private TextView seatsLabelTextView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        // Set layout
        timeTextView = (TextView) findViewById(R.id.pickUp_time);
        seatsTextView = (TextView) findViewById(R.id.available_seats);
        divergenceTextView = (TextView) findViewById(R.id.divergence_pickUp);
        actualPriceTextView = (TextView) findViewById(R.id.your_price);
        regularPriceTextView = (TextView) findViewById(R.id.regular_price);
        seatsLabelTextView = (TextView) findViewById(R.id.label_seats);

        Intent intent = getIntent();
        addressStart = intent.getParcelableExtra("AddressStart");
        addressEnd = intent.getParcelableExtra("AddressEnd");
        taxiResult = (TaxiResult) intent.getSerializableExtra("TaxiResult");

        // Get colors
        int orangeColor = getResources().getColor(R.color.colorOrange);
        int greenColor = getResources().getColor(R.color.colorGreen);
        int redColor = getResources().getColor(R.color.colorRed);


        // Hinzufügen der verfügbaren Sitze
        int available_seats = taxiResult.getAvailableSeats();
        switch (available_seats){
            case 0: this.seatsTextView.setTextColor(redColor);
                this.seatsLabelTextView.setText("Seats");
                break;
            case 1: this.seatsTextView.setTextColor(orangeColor);
                this.seatsLabelTextView.setText("Seat");
                break;
            default: this.seatsTextView.setTextColor(greenColor);
                this.seatsLabelTextView.setText("Seats");
                break;
        }

        // Set color based on relative distance to input divergence
        float divergence = (float) DistanceCalculator.distance(addressStart.getLatitude(), addressStart.getLongitude(), taxiResult.getTaxiStartPosLat(), taxiResult.getTaxiStartPosLon(), "M");
        if (divergence <= 0.333 * ResultsActivity.divergence)
            this.divergenceTextView.setTextColor(greenColor);
        else if (divergence <= 0.666 * ResultsActivity.divergence)
            this.divergenceTextView.setTextColor(orangeColor);
        else
            this.divergenceTextView.setTextColor(redColor);

        this.divergenceTextView.setText(String.format("%.0f yards", divergence * ArrayAdapterTaxi.MILES_IN_YARDS));


        // Aufbereitung der Startzeit
        Calendar pickUpTime = taxiResult.getStartTime();
        SimpleDateFormat sf = new SimpleDateFormat(" h':'mm a");

        this.timeTextView.setText(sf.format(pickUpTime.getTime()));
        this.seatsTextView.setText(Integer.toString(available_seats));


        // Get and set values
        float actualPrice = taxiResult.getActualPrice();
        float regularPrice = taxiResult.getNormalPrice();
        actualPriceTextView.setText(Float.toString(actualPrice) + " $");
        regularPriceTextView.setText(Float.toString(regularPrice) + " $");

    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Check permission
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            // Request permission if not granted
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_REQUEST);

        } else {
            mMap.setMyLocationEnabled(true);
        }

        // Positions
        LatLng start = new LatLng(addressStart.getLatitude(), addressStart.getLongitude());
        LatLng end = new LatLng(addressEnd.getLatitude(), addressEnd.getLongitude());
        LatLng taxiStart = new LatLng(taxiResult.getTaxiStartPosLat(), taxiResult.getTaxiStartPosLon());

        // Request walking route
        Routing walkRouting = new Routing.Builder()
                .travelMode(AbstractRouting.TravelMode.WALKING)
                .withListener(new WalkRoutingListener())
                .waypoints(start, taxiStart)
                .build();

        walkRouting.execute();

        // Request taxi route
        Routing taxiRouting = new Routing.Builder()
                .travelMode(AbstractRouting.TravelMode.DRIVING)
                .withListener(new TaxiRoutingListener())
                .waypoints(taxiStart, end)
                .build();

        taxiRouting.execute();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case LOCATION_REQUEST: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    Log.d("Map", "Permission granted");

                    try {
                        mMap.setMyLocationEnabled(true);
                    } catch (SecurityException e) {
                        e.printStackTrace();
                    }

                } else {
                    Log.d("Map", "Permission NOT granted");
                }

                return;
            }
        }
    }

    protected class TaxiRoutingListener implements RoutingListener {

        @Override
        public void onRoutingFailure(RouteException e) {
            Toast.makeText(getApplicationContext(), "Taxi routing failed", Toast.LENGTH_LONG).show();
            Log.e("Taxi Routing", "Routing failed: " + e.getMessage());
            e.printStackTrace();
        }

        @Override
        public void onRoutingStart() {
        }

        @Override
        public void onRoutingSuccess(ArrayList<Route> routes, int shortestRouteIndex) {

            // Taxi route polyline
            PolylineOptions polylineOptions = new PolylineOptions();
            polylineOptions.color(Color.RED);
            polylineOptions.addAll(routes.get(shortestRouteIndex).getPoints());

            mMap.addPolyline(polylineOptions);

            // Set Markers
            LatLng taxiStart = new LatLng(taxiResult.getTaxiStartPosLat(), taxiResult.getTaxiStartPosLon());
            LatLng end = new LatLng(addressEnd.getLatitude(), addressEnd.getLongitude());
            MarkerOptions taxiMarker = new MarkerOptions()
                    .position(taxiStart)
                    .title("Taxi")
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE));

            mMap.addMarker(taxiMarker);
            mMap.addMarker(new MarkerOptions().position(end).title("Destination"));

            // Move camera to taxiStart point
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(taxiStart, 10));
        }

        @Override
        public void onRoutingCancelled() {
        }
    }

    protected class WalkRoutingListener implements RoutingListener {

        @Override
        public void onRoutingFailure(RouteException e) {
            Toast.makeText(getApplicationContext(), "Walk routing failed", Toast.LENGTH_LONG).show();
            Log.e("Walk Routing", "Routing failed: " + e.getMessage());
            e.printStackTrace();
        }

        @Override
        public void onRoutingStart() {
        }

        @Override
        public void onRoutingSuccess(ArrayList<Route> routes, int shortestRouteIndex) {

            // Walk route polyline
            PolylineOptions polylineOptions = new PolylineOptions();
            polylineOptions.color(Color.BLUE);
            polylineOptions.addAll(routes.get(shortestRouteIndex).getPoints());

            mMap.addPolyline(polylineOptions);

            // Add marker
            LatLng walkStart = new LatLng(addressStart.getLatitude(), addressStart.getLongitude());
            MarkerOptions startMarker = new MarkerOptions()
                    .position(walkStart)
                    .title("You")
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
            mMap.addMarker(startMarker);
        }

        @Override
        public void onRoutingCancelled() {
        }
    }
}
