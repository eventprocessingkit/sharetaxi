package de.ant.sharetaxi;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created for the seminar "Event Processing" SoSe 2016
 * at the Forschungszentrum Informatik (FZI) in Karlsruhe
 * by
 * Tim Bossenmaier
 * Nevena Nikolajevic and
 * Adrian Vetter
 *
 * A single result from the server describing a taxi route
 */
public class TaxiResult implements Serializable{

    private Calendar startTime;
    private int availableSeats;
    private float normalPrice;
    private float actualPrice;
    private float taxiStartPosLat;
    private float taxiStartPosLon;
    private float taxiEndPosLat;
    private float taxiEndPosLon;

    public static SimpleDateFormat SQL_DATETIME_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");


    /**
     * Constructs an object of this class with info from the server
     * @param msg the server message as String
     */
    public TaxiResult(String msg) {
        String[] data = msg.split(",");
        try {
            setStart(data[0]);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        setAvailableSeats(Integer.parseInt(data[1]));
        setNormalPrice(Float.parseFloat(data[2]));
        setActualPrice(Float.parseFloat(data[3]));

        setTaxiStartPosLat(Float.parseFloat(data[4]));
        setTaxiStartPosLon(Float.parseFloat(data[5]));
        setTaxiEndPosLat(Float.parseFloat(data[6]));
        setTaxiEndPosLon(Float.parseFloat(data[7]));
    }

    public Calendar getStartTime() {
        return startTime;
    }

    public void setStartTime(Calendar startTime) {
        this.startTime = startTime;
    }

    public void setStart(String start) throws ParseException {
        Calendar cal = Calendar.getInstance();
        cal.setTime(SQL_DATETIME_FORMAT.parse(start));
        setStartTime(cal);
    }

    public int getAvailableSeats() {
        return availableSeats;
    }

    public void setAvailableSeats(int availableSeats) {
        this.availableSeats = availableSeats;
    }

    public float getNormalPrice() {
        return normalPrice;
    }

    public void setNormalPrice(float normalPrice) {
        this.normalPrice = normalPrice;
    }

    public float getActualPrice() {
        return actualPrice;
    }

    public void setActualPrice(float actualPrice) {
        this.actualPrice = actualPrice;
    }

    public float getTaxiStartPosLat() {
        return taxiStartPosLat;
    }

    public void setTaxiStartPosLat(float taxiStartPosLat) {
        this.taxiStartPosLat = taxiStartPosLat;
    }

    public float getTaxiStartPosLon() {
        return taxiStartPosLon;
    }

    public void setTaxiStartPosLon(float taxiStartPosLon) {
        this.taxiStartPosLon = taxiStartPosLon;
    }

    public float getTaxiEndPosLat() {
        return taxiEndPosLat;
    }

    public void setTaxiEndPosLat(float taxiEndPosLat) {
        this.taxiEndPosLat = taxiEndPosLat;
    }

    public float getTaxiEndPosLon() {
        return taxiEndPosLon;
    }

    public void setTaxiEndPosLon(float taxiEndPosLon) {
        this.taxiEndPosLon = taxiEndPosLon;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("TaxiResult{");
        sb.append("startTime=").append(startTime);
        sb.append(", availableSeats=").append(availableSeats);
        sb.append(", normalPrice=").append(normalPrice);
        sb.append(", actualPrice=").append(actualPrice);
        sb.append(", taxiStartPosLat=").append(taxiStartPosLat);
        sb.append(", taxiStartPosLon=").append(taxiStartPosLon);
        sb.append(", taxiEndPosLat=").append(taxiEndPosLat);
        sb.append(", taxiEndPosLon=").append(taxiEndPosLon);
        sb.append('}');
        return sb.toString();
    }
}
