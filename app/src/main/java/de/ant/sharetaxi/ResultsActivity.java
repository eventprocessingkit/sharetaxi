package de.ant.sharetaxi;

import android.content.Intent;
import android.location.Address;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created for the seminar "Event Processing" SoSe 2016
 * at the Forschungszentrum Informatik (FZI) in Karlsruhe
 * by
 * Tim Bossenmaier
 * Nevena Nikolajevic and
 * Adrian Vetter
 *
 * The Activity for the results screen
 */
public class ResultsActivity extends AppCompatActivity {

    public Address addressStart;
    public Address addressEnd;
    public ArrayList<TaxiResult> taxiResults;
    public static float divergence;

    private TextView mFromTextView;
    private TextView mToTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Get results
        Intent intent = getIntent();
        taxiResults = (ArrayList<TaxiResult>)intent.getSerializableExtra("Results");
        addressStart = intent.getParcelableExtra("AddressStart");
        addressEnd = intent.getParcelableExtra("AddressEnd");
        divergence = intent.getFloatExtra("Divergence", 1000);
        String start = intent.getStringExtra("AddressStartString");
        String end = intent.getStringExtra("AddressEndString");

        // Set view
        setContentView(R.layout.activity_results);

        // Get views
        mFromTextView = (TextView)findViewById(R.id.fromText);
        mToTextView = (TextView)findViewById(R.id.toText);

        // Set Start/End
        mFromTextView.setText(start);
        mToTextView.setText(end);
    }
}
