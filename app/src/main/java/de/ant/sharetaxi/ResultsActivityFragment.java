package de.ant.sharetaxi;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;

/**
 * Created for the seminar "Event Processing" SoSe 2016
 * at the Forschungszentrum Informatik (FZI) in Karlsruhe
 * by
 * Tim Bossenmaier
 * Nevena Nikolajevic and
 * Adrian Vetter
 *
 * The Fragment in the results screen showing all taxi routes from the server
 */
public class ResultsActivityFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        ArrayList <TaxiResult> taxiResults = ((ResultsActivity)getActivity()).taxiResults;


        ArrayAdapterTaxi taxiListAdapter = new ArrayAdapterTaxi(
                                                    (ResultsActivity) getActivity(), // Die aktuelle Umgebung (diese Activity)
                                                    taxiResults); // Results as ArrayList

        View rootView = inflater.inflate(R.layout.fragment_result, container, false);

        ListView taxiListView = (ListView) rootView.findViewById(R.id.listViewTaxiList);
        taxiListView.setAdapter(taxiListAdapter);

        return rootView;
    }

}